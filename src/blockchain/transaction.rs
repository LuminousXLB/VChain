#![allow(unused_imports)]

use std::{
    cmp::{Eq, Ordering},
    collections::BTreeMap,
};

use chrono::{
    DateTime,
    serde::ts_milliseconds,
    Utc,
};
use hbbft::crypto::Signature;
use hex;
use rand::RngCore;
use serde::{Deserialize, Serialize};

use crate::{
    TransactionHash,
    utils::Hash256,
};

#[derive(Serialize, Deserialize, Debug, Hash, Clone)]
pub enum VChainTransactionData {
    Dummy(String)
}


#[derive(Serialize, Deserialize, Debug, Hash, Clone)]
pub struct VChainTransaction {
    #[serde(with = "ts_milliseconds")]
    pub timestamp: DateTime<Utc>,
    pub digest: TransactionHash,
    pub data: VChainTransactionData,
}


impl VChainTransaction {
    pub fn new(data: VChainTransactionData) -> Self {
        let digest = Hash256::sha3_256(bincode::serialize(&data).unwrap().as_slice());

        Self {
            timestamp: Utc::now(),
            digest,
            data,
        }
    }

    pub fn random(length: usize) -> Self {
        let mut data: Vec<u8> = vec![0; length];
        let mut rng = rand::thread_rng();
        rng.fill_bytes(data.as_mut_slice());

        VChainTransaction::new(VChainTransactionData::Dummy(hex::encode(data.as_slice())))
    }
}

impl PartialEq for VChainTransaction {
    fn eq(&self, other: &Self) -> bool {
        self.digest == other.digest
    }
}

impl Eq for VChainTransaction {}

impl PartialOrd for VChainTransaction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.digest.partial_cmp(&other.digest)
    }
}

impl Ord for VChainTransaction {
    fn cmp(&self, other: &Self) -> Ordering {
        self.digest.cmp(&other.digest)
    }
}
