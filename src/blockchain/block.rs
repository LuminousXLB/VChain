use std::collections::BTreeSet;

use chrono::{
    DateTime,
    serde::ts_milliseconds,
    Utc,
};
use hbbft::crypto::{PublicKey, SecretKey, Signature};
use serde::{Deserialize, Serialize};

use crate::{
    blockchain::transaction::VChainTransaction,
    BlockHash,
    utils::Hash256,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct VChainBlock {
    block_hash: BlockHash,
    body: VChainBlockBody,
    signature: Signature,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct VChainBlockBody {
    header: VChainBlockHeader,
    payload: VChainBlockPayload,
    proposer: PublicKey,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct VChainBlockHeader {
    #[serde(with = "ts_milliseconds")]
    pub timestamp: DateTime<Utc>,
    pub block_height: u64,
    pub epoch_id: u64,
    pub last_block_hash: Hash256,
}

impl VChainBlockHeader {
    pub fn new(block_height: u64, epoch_id: u64, last_block_hash: Hash256) -> Self {
        Self {
            timestamp: Utc::now(),
            block_height,
            epoch_id,
            last_block_hash,
        }
    }
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum VChainBlockPayload {
    CodeBase,
    Transactions(BTreeSet<VChainTransaction>),
}

impl VChainBlock {
    pub fn new(header: VChainBlockHeader, payload: VChainBlockPayload, proposer_sk: &SecretKey) -> Self {
        let proposer = proposer_sk.public_key();
        let body = VChainBlockBody { header, payload, proposer };

        let data = bincode::serialize(&body).unwrap();

        Self {
            block_hash: Hash256::sha3_256(data.as_slice()),
            body,
            signature: proposer_sk.sign(data.as_slice()),
        }
    }

    pub fn block_hash(&self) -> &BlockHash {
        &self.block_hash
    }

    pub fn height(&self) -> u64 {
        self.body.header.block_height
    }

    pub fn epoch(&self) -> u64 {
        self.body.header.epoch_id
    }

    pub fn payload(&self) -> &VChainBlockPayload {
        &self.body.payload
    }

    pub fn contains(&self, transaction: &VChainTransaction) -> bool {
        match &self.body.payload {
            VChainBlockPayload::CodeBase => false,
            VChainBlockPayload::Transactions(transactions) => transactions.contains(transaction),
        }
    }
}
