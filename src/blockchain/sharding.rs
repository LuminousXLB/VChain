use std::{
    collections::{
        BinaryHeap,
        BTreeMap,
        HashSet,
    },
    iter::Iterator,
};

use merkle::MerkleTree;
use serde::{
    Deserialize,
    Serialize,
};
use tiny_keccak::{Hasher, Sha3};

use crate::{
    blockchain::{
        block::VChainBlock,
        transaction::VChainTransaction,
    },
    BlockHash,
    ConsensusId,
    NodeId,
    TransactionHash,
};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ShardMessage {
    Client(VChainTransaction),
    Route((u64, VChainTransaction)),
    Consensus((ConsensusId, HashSet<VChainTransaction>)),
    Block(VChainBlock),
}

fn digest_compress(digest: &[u8]) -> u128 {
    let mut sum: u128 = 0;
    digest.as_ref().chunks(16).for_each(|chunk| {
        let mut buf: [u8; 16] = Default::default();
        buf.clone_from_slice(chunk);

        sum = sum.wrapping_add(u128::from_be_bytes(buf));
    });

    sum
}

pub fn compute_shard_number(node_num: u64) -> u64 {
    (node_num as f64).sqrt().floor() as u64
}

pub fn compute_node_ascription(node_id: &NodeId, epoch_id: u64, shard_num: u64) -> u64 {
    assert_ne!(shard_num, 0);

    // calculate which shard the node belongs to
    let mut hash_ctx = Sha3::v256();
    hash_ctx.update(node_id.as_ref());
    hash_ctx.update(&epoch_id.to_be_bytes());
    hash_ctx.update(&shard_num.to_be_bytes());

    let mut output = [0u8; 32];
    hash_ctx.finalize(output.as_mut());

    let sum = digest_compress(output.as_ref());
    sum.rem_euclid(shard_num as u128) as u64
}

pub fn compute_sharding(nodes: Vec<&NodeId>, epoch_id: u64, shard_num: u64) -> BTreeMap<u64, BinaryHeap<NodeId>> {
    assert_ne!(shard_num, 0);

    let mut shards_nodes_map: BTreeMap<u64, BinaryHeap<NodeId>> = BTreeMap::new();

    nodes.into_iter().for_each(|node_id| {
        let shard_index = compute_node_ascription(node_id, epoch_id, shard_num);
        match shards_nodes_map.get_mut(&shard_index) {
            None => {
                let mut shard = BinaryHeap::new();
                shard.push(node_id.clone());
                shards_nodes_map.insert(shard_index, shard);
            }
            Some(shard) => {
                shard.push(node_id.clone());
            }
        }
    });

    shards_nodes_map
}

pub fn compute_concerned_shard_size(node_num: u64) -> u64 {
    assert_ne!(node_num, 0);

    let shard_num = compute_shard_number(node_num);
    ((shard_num as f64) / 2.0).ceil() as u64
}

pub fn compute_transaction_assignment(epoch_id: u64, tx_hash: &TransactionHash, last_block_hash: &BlockHash, shard_num: u64) -> u64 {
    assert_ne!(shard_num, 0);

    // calculate the start index of concerned shards
    let mut hash_ctx = Sha3::v256();
    hash_ctx.update(&epoch_id.to_be_bytes());
    hash_ctx.update(tx_hash.as_ref());
    hash_ctx.update(last_block_hash.as_ref());
    hash_ctx.update(&shard_num.to_be_bytes());

    let mut output = [0u8; 32];
    hash_ctx.finalize(output.as_mut());

    let sum = digest_compress(output.as_ref());
    sum.rem_euclid(shard_num as u128) as u64
}

pub fn compute_concerned_shards_indices(base_shard_index: u64, node_num: u64) -> HashSet<u64> {
    assert_ne!(node_num, 0);

    let concerned_size = compute_concerned_shard_size(node_num);
    let shard_num = compute_shard_number(node_num);

    (base_shard_index..base_shard_index + concerned_size).map(|x| {
        x.rem_euclid(shard_num)
    }).collect()
}

pub fn compute_consensus_id(node_id_sorted: BinaryHeap<NodeId>) -> ConsensusId {
    let merkle_tree = MerkleTree::from_vec(
        &ring::digest::SHA256,
        node_id_sorted.into_sorted_vec(),
    );

    ConsensusId::from_slice(merkle_tree.root_hash().as_slice())
}

pub fn compute_transaction_representative(round_id: u64, shard_id: &ConsensusId, concerned_shards: &BinaryHeap<ConsensusId>, shard_size: u64) -> u64 {
    // calculate the representative who attends the second layer consensus
    let mut hash_ctx = Sha3::v256();
    hash_ctx.update(&round_id.to_be_bytes());
    hash_ctx.update(shard_id.as_ref());
    for concerned_shard_id in concerned_shards {
        hash_ctx.update(concerned_shard_id.as_ref());
    }

    let mut output = [0u8; 32];
    hash_ctx.finalize(output.as_mut());

    let sum = digest_compress(output.as_ref());
    sum.rem_euclid(shard_size as u128) as u64
}

pub fn compute_block_representative(epoch_id: u64, last_block_hash: &BlockHash, node_num: u64) -> u64 {
    assert_ne!(node_num, 0);

    // calculate the representative who generate the blocks
    let shard_num = compute_shard_number(node_num);

    let mut hash_ctx = Sha3::v256();
    hash_ctx.update(&epoch_id.to_be_bytes());
    hash_ctx.update(last_block_hash.as_ref());
    hash_ctx.update(&node_num.to_be_bytes());
    hash_ctx.update(&shard_num.to_be_bytes());

    let mut output = [0u8; 32];
    hash_ctx.finalize(output.as_mut());

    let sum = digest_compress(output.as_ref());
    sum.rem_euclid(node_num as u128) as u64
}
