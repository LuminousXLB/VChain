use std::{
    cmp::Ordering,
    fmt::{Debug, Formatter, Result},
};

use hbbft::util::fmt_hex;
use rand::{
    distributions::Standard,
    prelude::*,
};
use serde::{Deserialize, Serialize};
use tiny_keccak::{Hasher, Sha3};

#[derive(Serialize, Deserialize, Clone, Copy, Hash, Eq)]
pub struct Hash256 {
    h: [u8; 32],
}

impl Hash256 {
    pub fn from_slice(data: &[u8]) -> Self {
        let mut h: [u8; 32] = Default::default();
        h.clone_from_slice(data);

        Self { h }
    }

    pub fn sha3_256(data: &[u8]) -> Self {
        let mut context = Sha3::v256();
        context.update(data);

        let mut h: [u8; 32] = Default::default();
        context.finalize(h.as_mut());

        Hash256 { h }
    }
}

impl AsRef<[u8]> for Hash256 {
    fn as_ref(&self) -> &[u8] {
        self.h.as_ref()
    }
}

impl Distribution<Hash256> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Hash256 {
        let mut h: [u8; 32] = Default::default();
        rng.fill_bytes(&mut h);
        Hash256 { h }
    }
}

impl Default for Hash256 {
    fn default() -> Self {
        Self {
            h: Default::default()
        }
    }
}

impl Debug for Hash256 {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        fmt_hex(self, f)
    }
}

impl PartialEq for Hash256 {
    fn eq(&self, other: &Self) -> bool {
        self.h.eq(&other.h)
    }
}

impl PartialOrd for Hash256 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.h.partial_cmp(&other.h)
    }
}

impl Ord for Hash256 {
    fn cmp(&self, other: &Self) -> Ordering {
        self.h.cmp(&other.h)
    }
}
