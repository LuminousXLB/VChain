//use hbbft::crypto::SecretKey;
//
//use crate::simulation::{SimReceiver, SimSender};
//
//pub struct SimulatedClient {
//    secret_key: SecretKey,
//    inbound_receiver: SimReceiver,
//    outbound_sender: SimSender,
//}
//
//impl SimulatedClient {
//    pub fn new(sk: SecretKey, in_recv: SimReceiver, out_send: SimSender) -> Self {
//        Self {
//            secret_key: sk,
//            inbound_receiver: in_recv,
//            outbound_sender: out_send,
//        }
//    }
//}
