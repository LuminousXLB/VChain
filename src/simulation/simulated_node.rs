use hbbft::crypto::{PublicKey, SecretKey, Signature};

use crate::{
    messaging::message::{OutboundMessage, TargetedMessage},
    node::handler::Handler,
    simulation::{SimMessage, SimReceiver, SimSender, SimTarget},
};
//use crate::messaging::message::SocketMessage;

#[derive(Clone)]
pub struct SimulatedNode {
    secret_key: SecretKey,
    handler: Handler,
    inbound_receiver: SimReceiver,
    outbound_sender: SimSender,
    initial_message: Vec<TargetedMessage>,
}

impl SimulatedNode {
    pub fn new(sk: SecretKey, handler: Handler, in_recv: SimReceiver, out_send: SimSender, init_msg: Option<TargetedMessage>) -> Self {
        let initial_message = match init_msg {
            Some(msg) => vec![msg],
            None => vec![],
        };

        Self {
            secret_key: sk,
            handler,
            inbound_receiver: in_recv,
            outbound_sender: out_send,
            initial_message,
        }
    }

    pub fn loop_run(&mut self) {
        for msg in self.initial_message.clone() {
            self.send(msg);
        }

        loop {
            let (_, msg) = self.inbound_receiver.recv().unwrap();

            let mut node = self.clone();
            std::thread::spawn(move || {
                let out_task = node.handle_message(msg.public_key, msg.signature, msg.message);
                out_task.into_iter().for_each(|task| {
                    node.send(task)
                });
            });

//            if let SocketMessage::ShardMessage(_) = msg.message.message {
//                let mut node = self.clone();
//                std::thread::spawn(move || {
//                    let out_task = node.handle_message(msg.public_key, msg.signature, msg.message);
//                    out_task.into_iter().for_each(|task| {
//                        node.send(task)
//                    });
//                });
//            } else {
//                let out_task = self.handle_message(msg.public_key, msg.signature, msg.message);
//                out_task.into_iter().for_each(|task| {
//                    self.send(task)
//                });
//            }
        }
    }
}

impl SimulatedNode {
    fn send(&mut self, targeted_message: TargetedMessage) {
        let outbound = OutboundMessage::new(targeted_message.message);
        let air = SimMessage::new(&self.secret_key, outbound);

        targeted_message.target.into_iter().for_each(|target| {
            self.outbound_sender.send((SimTarget::Address(target), air.clone())).unwrap();
        });
    }

    fn handle_message(&mut self, public_key: PublicKey, signature: Signature, outbound: OutboundMessage) -> Vec<TargetedMessage> {
        let verify = public_key.verify(
            &signature,
            bincode::serialize(&outbound).unwrap(),
        );

        if verify {
            self.handler.handle_socket_message(
                public_key,
                outbound.message,
                outbound.timestamp,
            )
        } else {
            vec![]
        }
    }
}
