use std::net::SocketAddr;

use crossbeam_channel::{Receiver, Sender};
use hbbft::crypto::{PublicKey, SecretKey, Signature};
use serde::{Deserialize, Serialize};

pub use message_scheduler::MessageScheduler;
// pub use simulated_client::SimulatedClient;
pub use simulated_dealer::loop_dealer;
pub use simulated_node::SimulatedNode;

use crate::{
    messaging::message::OutboundMessage,
    NodeId,
};

mod simulated_node;
mod message_scheduler;
mod simulated_dealer;
// mod simulated_client;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SimMessage {
    pub public_key: PublicKey,
    pub signature: Signature,
    pub message: OutboundMessage,
}

impl SimMessage {
    pub fn new(secret_key: &SecretKey, message: OutboundMessage) -> Self {
        let serialized = bincode::serialize(&message).unwrap();
        let signature = secret_key.sign(serialized);
        let public_key = secret_key.public_key();

        Self { public_key, signature, message }
    }
}

pub enum SimTarget {
    Address(SocketAddr),
    NodeId(NodeId),
}

pub type SimSender = Sender<(SimTarget, SimMessage)>;
pub type SimReceiver = Receiver<(SimTarget, SimMessage)>;
