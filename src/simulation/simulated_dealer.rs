use crate::{
    dealer::VChainDealer,
    messaging::message::OutboundMessage,
    NodeId,
    simulation::{
        SimMessage,
        SimReceiver,
        SimSender,
        SimTarget,
    },
};

pub fn loop_dealer(dealer: VChainDealer, inbound_receiver: SimReceiver, outbound_channel: SimSender) {
    loop {
        let (_, msg) = inbound_receiver.recv().unwrap();

        // let dealer_cloned = dealer.clone();
        // let o_channel = outbound_channel.clone();
        // std::thread::spawn(move|| {
        //     let resp = dealer_cloned.handle(&msg.public_key, msg.message.message);
        //
        //     let outbound = OutboundMessage::new(resp);
        //     let air = SimMessage::new(&dealer_cloned.secret_key, outbound);
        //
        //     let target_id = NodeId::from_pubkey(&msg.public_key);
        //
        //     o_channel.send((SimTarget::NodeId(target_id), air)).unwrap();
        // });

        // try do it in a single thread way
        // let dealer_cloned = dealer.clone();
        let o_channel = outbound_channel.clone();

        let resp = dealer.handle(&msg.public_key, msg.message.message);

        let outbound = OutboundMessage::new(resp);
        let air = SimMessage::new(&dealer.secret_key, outbound);

        let target_id = NodeId::from_pubkey(&msg.public_key);

        o_channel.send((SimTarget::NodeId(target_id), air)).unwrap();
    }
}
