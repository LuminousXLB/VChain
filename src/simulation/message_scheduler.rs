use std::{
    collections::{
        BTreeMap,
        HashMap,
    },
    net::SocketAddr,
    sync::{Arc, RwLock},
    time::Duration,
};
use std::str::FromStr;

use crossbeam_channel::TryRecvError;
use hbbft::crypto::SecretKey;
use log::*;
use rand::seq::IteratorRandom;
use rand::thread_rng;

use crate::{
    blockchain::{
        sharding::ShardMessage,
        transaction::VChainTransaction,
    },
    messaging::message::{OutboundMessage, SocketMessage},
    NodeId,
    simulation::{
        SimMessage,
        SimReceiver,
        SimSender,
        SimTarget,
    },
};

#[derive(Clone)]
pub struct MessageScheduler {
    client_secret_key: SecretKey,
    entity_map: Arc<RwLock<BTreeMap<NodeId, SocketAddr>>>,
    route_in_map: Arc<RwLock<HashMap<NodeId, SimReceiver>>>,
    route_out_map: Arc<RwLock<HashMap<SocketAddr, SimSender>>>,
}

impl MessageScheduler {
    pub fn new(client_secret_key: SecretKey) -> Self {
        Self {
            client_secret_key,
            entity_map: Arc::new(RwLock::new(BTreeMap::new())),
            route_in_map: Arc::new(RwLock::new(HashMap::new())),
            route_out_map: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub fn register(&mut self, node_id: NodeId, sock_addr: SocketAddr) -> (SimSender, SimReceiver) {
        let (tx1, rx1) = crossbeam_channel::unbounded();
        let (tx2, rx2) = crossbeam_channel::unbounded();
        {
            let mut out_lock = self.route_out_map.write().unwrap();
            {
                let mut in_lock = self.route_in_map.write().unwrap();
                {
                    let mut en_lock = self.entity_map.write().unwrap();
                    en_lock.insert(node_id, sock_addr);
                }
                in_lock.insert(node_id, rx1);
            }
            out_lock.insert(sock_addr, tx2);
        }

        (tx1, rx2)
    }

    pub fn send_transaction(&mut self, transaction: VChainTransaction) {
        let socket_message = SocketMessage::ShardMessage(ShardMessage::Client(transaction));
        let outbound_message = OutboundMessage::new(socket_message);
        let sim_message = SimMessage::new(&self.client_secret_key, outbound_message);

        let ser = bincode::serialize(&sim_message).unwrap();
        {
            let out_lock = self.route_out_map.read().unwrap();
            let (mut target_address, mut target_sender) = out_lock.iter().choose(&mut thread_rng()).unwrap().clone();

            while target_address == &SocketAddr::from_str("127.0.0.1:5000").unwrap() {
                let t = out_lock.iter().choose(&mut thread_rng()).unwrap().clone();
                target_address = t.0;
                target_sender = t.1;
            }

            match target_sender.send((SimTarget::Address(target_address.clone()), sim_message)) {
                Ok(_) => {
                    // Do nothing
                }
                Err(err) => {
                    panic!("Failed to send message: {:?}", err);
                }
            }

            // TODO: raise the log level to INFO to measure the performance
            trace!("Simulate Socket: From {:?} To {:?} Length {}", "{Client}", &target_address, ser.len());
        }
    }

    pub fn loop_schedule(&mut self) {
        let mut unit = 1;

        loop {
            let mut flag = false;

            let in_lock = self.route_in_map.read().unwrap();
            in_lock.iter().for_each(|(id, rx)| {
                match rx.try_recv() {
                    Ok((target, message)) => {
                        let target_addr = match target {
                            SimTarget::Address(addr) => addr,
                            SimTarget::NodeId(id) => {
                                let en_lock = self.entity_map.read().unwrap();
                                en_lock.get(&id).unwrap().to_owned()
                            }
                        };

                        flag = true;
                        let ser = bincode::serialize(&message).unwrap();

                        {
                            let out_lock = self.route_out_map.read().unwrap();
                            let tx = out_lock.get(&target_addr).expect(
                                format!("Socket channel to {:?} is disconnected", target_addr).as_str()
                            );
                            tx.send((target, message)).unwrap();
                        }

                        // TODO: raise the log level to INFO to measure the performance
                        info!("Simulate Socket: From {:?} To {:?} Length {}", &id, &target_addr, ser.len());
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => {
                        panic!("Socket channel from {:?} is disconnected", id);
                    }
                }
            });

            if flag {
                unit = 1;
            } else {
                unit = unit * 2;
                std::thread::sleep(Duration::from_millis(unit))
            }
        }
    }
}

