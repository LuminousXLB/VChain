use std::collections::BTreeSet;

use hbbft::crypto::PublicKeySet;
use merkle::{MerkleTree, Proof};
use serde::{Deserialize, Serialize};

use ring;

use crate::{Hash256, messaging::message::SecretKeyShareSerializable, NodeId};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum DealerMessage {
    KeyRequest(KeyRequest),
    KeyResponse(Result<KeyResponse, String>),
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct KeyRequest {
    pub count: usize,
    pub proof: Proof<NodeId>,
}

impl KeyRequest {
    pub fn count(&self) -> &usize {
        &self.count
    }

    pub fn proof(&self) -> &Proof<NodeId> {
        &self.proof
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct KeyResponse {
    pub group_root_hash: Hash256,
    pub index: usize,
    pub secret_key_share: SecretKeyShareSerializable,
    pub public_key_set: PublicKeySet,
}

impl KeyRequest {
    pub fn new(our_id: NodeId, mut node_ids: BTreeSet<NodeId>) -> Self {
        if !node_ids.contains(&our_id) {
            node_ids.insert(our_id);
        }

        let node_id_sorted = node_ids.into_iter().collect();

        let merkle_tree = MerkleTree::from_vec(
            &ring::digest::SHA256,
            node_id_sorted,
        );

        let count = merkle_tree.count();

        let proof = merkle_tree
            .gen_proof(our_id)
            .expect("The public key input doesn't exists in the merkle tree");

        KeyRequest { count, proof }
    }
}


