use hbbft::crypto::error::FromBytesError;
use redis::RedisError;

#[derive(Debug)]
pub enum ErrorKind {
    RedisError(RedisError),
    VerifyMerkleTreeFailed,
    PersistingKeySetFailed,
    DeserializePublicKeyFailed(FromBytesError),
    PubkeyDoesntMatchToProofValue,
}