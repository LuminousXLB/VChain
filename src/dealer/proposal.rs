use hbbft::crypto::PublicKey;
use merkle::Proof;

use crate::{Hash256, NodeId};

use super::error::ErrorKind;

pub struct KeyProposal {
    root_hash: Hash256,
    index: usize,
    threshold: usize,
}

impl KeyProposal {
    pub fn new(public_key: &PublicKey, count: &usize, proof: &Proof<NodeId>) -> Result<KeyProposal, ErrorKind> {
        let our_id = NodeId::from_pubkey(public_key);

        if proof.value != our_id {
            Err(ErrorKind::PubkeyDoesntMatchToProofValue)
        } else {
            match proof.validate(&proof.root_hash) {
                true => {
                    let index = proof.index(*count);
                    let threshold = (count - 1) / 3;

                    Ok(KeyProposal {
                        root_hash: Hash256::from_slice(proof.root_hash.as_slice()),
                        index,
                        threshold,
                    })
                }
                false => Err(ErrorKind::VerifyMerkleTreeFailed)
            }
        }
    }

    pub fn root_hash(&self) -> &Hash256 {
        &self.root_hash
    }

    pub fn index(&self) -> usize {
        self.index.clone()
    }

    pub fn threshold(&self) -> usize {
        self.threshold.clone()
    }
}
