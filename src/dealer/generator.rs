use hbbft::crypto::{
    poly::Poly,
    PublicKeySet,
    SecretKeySet,
};
use rand::Rng;
use redis::{Client, Commands, RedisResult, ToRedisArgs};
use serde::{Deserialize, Serialize};

use crate::{
    dealer::{
        error::ErrorKind,
        message::KeyResponse,
        proposal::KeyProposal,
    },
    messaging::message::SecretKeyShareSerializable,
};

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug, Clone)]
pub struct KeySetPersistence {
    poly: Poly,
    public_key_set: PublicKeySet,
}

impl KeySetPersistence {
    pub fn random<R: Rng>(threshold: usize, rng: &mut R) -> Self {
        let poly = Poly::random(threshold, rng);

        let secret_key_set = SecretKeySet::from(poly.clone());
        let public_key_set = secret_key_set.public_keys();

        KeySetPersistence { poly, public_key_set }
    }
}


pub struct KeyGenerator {
    client: Client
}

impl KeyGenerator {
    pub fn new(uri: &str) -> KeyGenerator {
        KeyGenerator {
            client: Client::open(uri)
                .expect("Fail to open redis server"),
        }
    }

    pub fn generate<R: Rng>(&self, request: KeyProposal, rng: &mut R) -> Result<KeyResponse, ErrorKind> {
        let root_hash = request.root_hash().as_ref().clone();

        let opt_key_set = match self.exists(root_hash) {
            Ok(true) => {
                match self.query(root_hash) {
                    Ok(ks) => Ok(ks),
                    Err(e) => Err(ErrorKind::RedisError(e)),
                }
            }
            Ok(false) => {
                let ks = KeySetPersistence::random(request.threshold(), rng);

                match self.insert(
                    request.root_hash().as_ref().clone()
                    , ks.clone(),
                ) {
                    Ok(true) => Ok(ks),
                    Ok(false) => Err(ErrorKind::PersistingKeySetFailed),
                    Err(e) => Err(ErrorKind::RedisError(e)),
                }
            }
            Err(e) => Err(ErrorKind::RedisError(e)),
        };

        match opt_key_set {
            Ok(KeySetPersistence { poly, public_key_set }) => {
                let secret_key_set = SecretKeySet::from(poly);
                let secret_key_share = secret_key_set.secret_key_share(request.index());

                Ok(KeyResponse {
                    group_root_hash: request.root_hash().clone(),
                    index: request.index(),
                    secret_key_share: SecretKeyShareSerializable::new(secret_key_share),
                    public_key_set,
                })
            }
            Err(e) => Err(e)
        }
    }

    fn exists<K: ToRedisArgs>(&self, root_hash: K) -> RedisResult<bool> {
        let mut con = self.client.get_connection()?;
        con.exists(root_hash)
    }

    fn insert<K: ToRedisArgs>(&self, root_hash: K, key_set: KeySetPersistence) -> RedisResult<bool> {
        let mut con = self.client.get_connection()?;
        let serialized = bincode::serialize(&key_set)
            .expect("Fail to serialize key set");
        let ret: String = con.set(root_hash, serialized)?;

        Ok(ret == String::from("OK"))
    }

    fn query<K: ToRedisArgs>(&self, root_hash: K) -> RedisResult<KeySetPersistence> {
        let mut con = self.client.get_connection()?;
        let ret: Vec<u8> = con.get(root_hash)?;
        let deserialized: KeySetPersistence = bincode::deserialize(ret.as_slice())
            .expect("Fail to deserialize key set");

        Ok(deserialized)
    }
}
