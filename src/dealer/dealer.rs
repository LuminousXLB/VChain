use hbbft::crypto::{PublicKey, SecretKey};
use log::*;
use rand::thread_rng;

use crate::{
    dealer::{KeyGenerator, KeyProposal, message::DealerMessage},
    messaging::message::SocketMessage,
    NodeId,
};

#[derive(Clone)]
pub struct VChainDealer {
    pub secret_key: SecretKey,
    redis_address: String,
}

impl VChainDealer {
    pub fn new(secret_key: SecretKey, redis_address: String) -> Self {
        Self { secret_key, redis_address }
    }

    pub fn handle(&self, sender_pk: &PublicKey, message: SocketMessage) -> SocketMessage {
        let dealer_resp = match message {
            SocketMessage::DealerMessage(dealer_message) => {
                self.distribute_key(sender_pk, dealer_message)
            }
            _ => {
                error!("Invalid Message {:?}: {:?}", NodeId::from_pubkey(sender_pk), message);
                unreachable!()
            }
        };

        SocketMessage::DealerMessage(dealer_resp)
    }

    fn distribute_key(&self, sender_pk: &PublicKey, message: DealerMessage) -> DealerMessage {
        let mut rng = thread_rng();

        match message {
            DealerMessage::KeyRequest(request) => {
                let proposal_result = KeyProposal::new(
                    sender_pk,
                    &request.count,
                    &request.proof,
                );

                match proposal_result {
                    Ok(proposal) => {
                        let generator = KeyGenerator::new(self.redis_address.as_str());
                        match generator.generate(proposal, &mut rng) {
                            Ok(key_info) => DealerMessage::KeyResponse(Ok(key_info)),
                            Err(err) => DealerMessage::KeyResponse(Err(format!("{:?}", err)))
                        }
                    }
                    Err(err) => {
                        DealerMessage::KeyResponse(Err(format!("{:?}", err)))
                    }
                }
            }
            DealerMessage::KeyResponse(_) => {
                unreachable!()
            }
        }
    }
}