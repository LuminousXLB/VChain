use std::collections::{BTreeSet, HashSet};
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::time::Duration;

use chrono::{DateTime, Utc};
use hbbft::{
    crypto::PublicKey,
    dynamic_honey_badger::Message,
    queueing_honey_badger::{Batch, Result, Step},
    Target,
};
use log::*;

use crate::{
    blockchain::{
        block::{VChainBlock, VChainBlockHeader, VChainBlockPayload},
        sharding::{
            compute_concerned_shards_indices,
            compute_node_ascription,
            compute_shard_number,
            compute_transaction_assignment,
            ShardMessage,
        },
        sharding::compute_block_representative,
        transaction::VChainTransaction,
    },
    ConsensusId,
    dealer::message::DealerMessage,
    messaging::message::SocketMessage,
    node::{
        consensus::ConsensusMessage,
        handler::{Handler, Machine},
        SocketTarget,
        SocketTransmitTask,
    },
    NodeId,
    utils::Hash256,
};

// Consensus Phase
impl Handler {
    pub fn handle_dealer_message(&mut self, _sender_pk: PublicKey, message: DealerMessage, _timestamp: DateTime<Utc>) -> Vec<SocketTransmitTask> {
        loop {
            match *self.machine.read().unwrap() {
                Machine::Validator(_) => {
                    break;
                }
                _ => {
                    debug!("{:?} Wait for state change @ {} {}", self.our_id, module_path!(), line!());
                    std::thread::sleep(Duration::from_secs(1));
                }
            }
        }

        let result = match message {
            DealerMessage::KeyRequest(_) => {
                unreachable!("Invalid Receiving Key Request!");
            }
            DealerMessage::KeyResponse(response) => {
                match response {
                    Ok(response) => {
                        match &mut *self.machine.write().unwrap() {
                            Machine::Validator(validator) => {
                                let instance_id = response.group_root_hash.clone();
                                let result = validator.handle_keygen_response(response);
                                Some((instance_id, result))
                            }
                            _ => {
                                error!("{:?} Receive dealer response with invalid state {:?}", self.our_id, &self.machine);
                                unreachable!()
                            }
                        }
                    }
                    Err(err) => {
                        warn!("Error Request Key Share: {}", err);
                        None
                    }
                }
            }
        };

        match result {
            Some((consensus_id, step_option)) => {
                match step_option {
                    Some(step) => self.handle_step(consensus_id, step),
                    None => { vec![] }
                }
            }
            None => { vec![] }
        }
    }

    pub fn handle_consensus_message(&mut self, sender_id: NodeId, consensus_id: ConsensusId, message: Message<NodeId>) -> Vec<SocketTransmitTask> {
        let result = {
            match &mut *self.machine.write().unwrap() {
                Machine::Validator(validator) => {
                    let mut validator_cloned = validator.clone();
                    validator_cloned.handle_message(&consensus_id, sender_id, message)
                }
                _ => unreachable!(),
            }
        };

        self.handle_message_result(consensus_id, result)
    }
}

impl Handler {
    pub fn handle_shard_message(&mut self, shard_message: ShardMessage) -> Vec<SocketTransmitTask> {
        loop {
            match *self.machine.read().unwrap() {
                Machine::Validator(_) => {
                    break;
                }
                _ => {
                    debug!("{:?} Wait for state change @ {} {}", self.our_id, module_path!(), line!());
                    std::thread::sleep(Duration::from_secs(1));
                }
            }
        }

        match shard_message {
            ShardMessage::Client(transaction) => {
                self.handle_shard_client_message(transaction)
            }
            ShardMessage::Route((epoch, transaction)) => {
                self.handle_shard_route_message(epoch, transaction)
            }
            ShardMessage::Consensus((consensus_id, transactions)) => {
                self.handle_shard_consensus_message(consensus_id, transactions)
            }
            ShardMessage::Block(block) => {
                {
                    let mut lock = self.blocks.write().unwrap();
                    lock.push_back(block.clone());
                }
                {
                    if let VChainBlockPayload::Transactions(block_transactions) = block.payload() {
                        let mut lock = self.machine.write().unwrap();
                        match &mut *lock {
                            Machine::Validator(validator) => {
                                let mut lock = validator.prepared_for_block.lock().unwrap();

                                block_transactions.iter().for_each(|tx| {
                                    if lock.contains(tx) {
                                        lock.remove(tx);
                                    }
                                });

                                lock.clear();
                            }
                            _ => {}
                        };
                    }
                }
                info!("{:?} BLOCK RECEIVED {:?}", self.our_id, block);

                match OpenOptions::new().write(true).create(true).open(format!("{:?}.log", self.our_id)) {
                    Ok(mut f) => {
                        let lock = self.blocks.read().unwrap();
                        lock.iter().for_each(|block| {
                            if let VChainBlockPayload::Transactions(transactions) = block.payload() {
                                if !(transactions.is_empty()) {
                                    f.write_fmt(format_args!("{:#?}\n", block)).expect("Error writing file");
                                }
                            }
                        });
                    }
                    Err(e) => {
                        error!("Error open file {}", e);
                    }
                }

                let node_ids: Vec<_> = self.nodes().into_sorted_vec();
                assert!(!node_ids.is_empty());

                let node_idx = compute_block_representative(
                    self.epoch(),
                    &block.block_hash().clone(),
                    node_ids.len() as u64,
                );

                if node_ids[node_idx as usize] == self.our_id {
                    info!("{:?} MY TURN TO OUTPUT BLOCK", self.our_id);
                    let mut transactions = BTreeSet::new();

                    for idx in 0..2 {
                        let mut lock = self.machine.write().unwrap();

                        match &mut *lock {
                            Machine::Validator(validator) => {
                                let mut lock = validator.prepared_for_block.lock().unwrap();

                                lock.iter().for_each(|tx| {
                                    if !block.contains(tx) {
                                        transactions.insert(tx.clone());
                                    }
                                });

                                lock.clear();
                            }
                            _ => {}
                        };

                        if transactions.is_empty() {
                            info!("{:?} OUTPUT BLOCK WAITING {}", self.our_id, idx);
                            std::thread::sleep(Duration::from_secs(2));
                        } else {
                            break;
                        }

                        assert!(
                            match &mut *lock {
                                Machine::Validator(validator) => {
                                    let lock = validator.prepared_for_block.lock().unwrap();
                                    lock.is_empty()
                                }
                                _ => true
                            }
                        )
                    }

                    let header = VChainBlockHeader::new(block.height() + 1, self.epoch(), block.block_hash().clone());

                    let payload = VChainBlockPayload::Transactions(transactions);

                    let block = VChainBlock::new(header, payload, &self.our_sk);
                    info!("{:?} OUTPUT BLOCK {:?}", self.our_id, block);

                    vec![
                        SocketTransmitTask {
                            target: SocketTarget::MultiNodeId(node_ids),
                            message: SocketMessage::ShardMessage(ShardMessage::Block(block)),
                        }
                    ]
                } else {
                    vec![]
                }
            }
        }
    }

    pub fn handle_shard_client_message(&mut self, transaction: VChainTransaction) -> Vec<SocketTransmitTask> {
        let node_num = self.node_number();
        let shard_num = compute_shard_number(node_num);
        let nodes = self.nodes();
        let epoch_id = self.epoch();

        assert_ne!(shard_num, 0);
        let shard_index = compute_transaction_assignment(
            epoch_id,
            &transaction.digest,
            &Hash256::default(), // FIXME: Find the block hash
            shard_num,
        );

        let shards = compute_concerned_shards_indices(shard_index, node_num);


        let targets: Vec<NodeId> = nodes.into_iter().filter(|id| {
            let ascription = compute_node_ascription(id, epoch_id, shard_num);
            shards.contains(&ascription)
        }).collect();

        info!("Route Message ({:?})", &transaction.digest);
//        info!("Route Message ({:?}) to shards {:?} -> {:?}", &transaction.digest, shards, targets);

        let task = SocketTransmitTask::new(
            SocketTarget::MultiNodeId(targets),
            SocketMessage::ShardMessage(ShardMessage::Route((epoch_id, transaction))),
        );

        vec![task]
    }

    pub fn handle_shard_route_message(&mut self, _epoch_id: u64, transaction: VChainTransaction) -> Vec<SocketTransmitTask> {
        // TODO: Confirm that I should process this transaction
        // TODO: Validate the transaction message
        // TODO: Sign the transaction message
        // TODO: Propose to consensus

        let (result, consensus_id) = match &mut *self.machine.write().unwrap() {
            Machine::Validator(validator) => {
                (validator.propose(transaction), validator.our_shard_id.clone())
            }
            _ => {
                error!("Received Route message but state is not Validator @ {} {}", module_path!(), line!());
                unreachable!();
            }
        };

        self.handle_message_result(consensus_id, result)
    }

    pub fn handle_shard_consensus_message(&mut self, consensus_id: ConsensusId, transactions: HashSet<VChainTransaction>) -> Vec<SocketTransmitTask> {
        let message = match &mut *self.machine.write().unwrap() {
            Machine::Validator(validator) => {
                validator.handle_2nd_layer_consensus_output(&consensus_id, transactions)
            }
            _ => {
                error!("Received Consensus message but state is not Validator @ {} {}", module_path!(), line!());
                unreachable!();
            }
        };

        match message {
            None => vec![],
            Some(message) => vec![
                SocketTransmitTask::new(
                    SocketTarget::Dealer,
                    SocketMessage::DealerMessage(message),
                )
            ],
        }
    }
}

impl Handler {
    fn handle_message_result(&mut self, consensus_id: ConsensusId, result: Option<Result<Step<VChainTransaction, NodeId>>>) -> Vec<SocketTransmitTask> where {
        match result {
            None => {
                debug!("{:?}: The QHB instance {:?} hasn't been established or the consensus doesn't exist", self.our_id, consensus_id);
                vec![]
            }
            Some(Ok(step)) => { self.handle_step(consensus_id, step) }
            Some(Err(err)) => {
                warn!("Error handling DHB message: {:?}", err);
                vec![]
            }
        }
    }

    pub fn handle_step(&mut self, consensus_id: ConsensusId, step: Step<VChainTransaction, NodeId>) -> Vec<SocketTransmitTask> where {
        debug!(" - | {:?}: {:?} => {:?}", &self.our_id, &consensus_id, &step);

        step.fault_log.0.iter().for_each(|fault| {
            warn!("{:?}: Fault log: {:?} - {:?}", self.our_id, consensus_id, fault);
        });

        let mut tasks: Vec<SocketTransmitTask> = step.messages.iter().map(|message| {
            let our_id = self.our_id.clone();

            let target = match message.target {
                Target::All => {
                    match &*self.machine.read().unwrap() {
                        Machine::Validator(validator) => {
                            let targets = validator.consensus_candidates(&consensus_id).into_iter()
                                .filter(|id| {
                                    id != &our_id
                                }).collect();

                            SocketTarget::MultiNodeId(targets)
                        }
                        _ => unreachable!()
                    }
                }
                Target::Node(target_id) => {
                    SocketTarget::NodeId(target_id)
                }
            };

            SocketTransmitTask::new(target, SocketMessage::ConsensusMessage(ConsensusMessage {
                consensus_id,
                dhb_message: message.message.to_owned(),
            }))
        }).collect();

        tasks.extend(self.handle_output(&consensus_id, step.output).into_iter());

        tasks
    }

    pub fn handle_output(&mut self, consensus_id: &ConsensusId, output: Vec<Batch<VChainTransaction, NodeId>>) -> Vec<SocketTransmitTask> {
//        vec![]
        if output.is_empty() {
            vec![]
        } else {
            let mut validator_cloned = match &mut *self.machine.write().unwrap() {
                Machine::Validator(validator) => {
                    validator.clone()
                }
                _ => unreachable!(),
            };

            if consensus_id == &validator_cloned.our_shard_id {
                let steps = validator_cloned.handle_1st_layer_consensus_output(output);
                steps.into_iter().flat_map(|(consensus_id, step)| {
                    self.handle_message_result(consensus_id, Some(step)).into_iter()
                }).collect()
            } else {
                // FIXME: Merge transactions
                let transactions = output.iter().flat_map(|batch| {
                    batch.contributions().flat_map(|(_node_id, transactions)| {
                        transactions.iter().cloned()
                    })
                }).collect();

                // Broadcast to my shards
                let candidates = validator_cloned.consensus_candidates(&consensus_id);
                let message = ShardMessage::Consensus((consensus_id.clone(), transactions));

                vec![
                    SocketTransmitTask::new(
                        SocketTarget::MultiNodeId(candidates),
                        SocketMessage::ShardMessage(message),
                    )
                ]
            }
        }
    }
}