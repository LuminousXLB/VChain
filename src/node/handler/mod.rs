use std::{
    collections::{
        BinaryHeap,
        BTreeMap,
        HashSet,
        VecDeque,
    },
    net::SocketAddr,
    sync::{
        Arc,
        atomic::{AtomicU64, Ordering},
        Mutex,
        RwLock,
    },
};

use chrono::{DateTime, Utc};
use hbbft::crypto::{PublicKey, SecretKey};
use log::*;

use crate::{
    blockchain::{
        block::VChainBlock,
        sharding::{compute_node_ascription, compute_shard_number},
    },
    messaging::message::{SocketMessage, TargetedMessage},
    node::{
        bootstrap::{BootstrapCenter, BootstrapPeer},
        consensus::
        ValidatorNode,
        SocketTarget,
    },
    NodeId,
};

mod bootstrap_phase;
mod consensus_phase;

#[derive(Clone, Debug)]
enum Machine {
    NotOperational,
    BootstrapCenter(Arc<Mutex<BootstrapCenter>>),
    BootstrapPeer(Arc<Mutex<BootstrapPeer>>),
    Validator(ValidatorNode),
}

#[derive(Clone)]
pub struct Handler {
    our_id: NodeId,
    our_pk: PublicKey,
    our_sk: SecretKey,
    epoch: Arc<AtomicU64>,
    bind_address: SocketAddr,
    dealer_address: SocketAddr,
    machine: Arc<RwLock<Machine>>,
    node_socket_map: Arc<RwLock<BTreeMap<NodeId, SocketAddr>>>,
    blocks: Arc<RwLock<VecDeque<VChainBlock>>>,
}


// Start Phase
impl Handler {
    pub fn new(our_sk: SecretKey, bind_address: SocketAddr, dealer_address: SocketAddr) -> Self {
        let our_pk = our_sk.public_key();
        let our_id = NodeId::from_pubkey(&our_pk);

        info!("Node ID: {:?} @ {}", &our_id, bind_address);

        Self {
            our_id,
            our_pk,
            our_sk,
            epoch: Arc::new(AtomicU64::new(0)),
            bind_address,
            dealer_address,
            machine: Arc::new(RwLock::new(Machine::NotOperational)),
            node_socket_map: Arc::new(RwLock::new(BTreeMap::new())),
            blocks: Arc::new(RwLock::new(VecDeque::new())),
        }
    }

    pub fn startup_as_bootstrap_center(&mut self, peers_id: HashSet<NodeId>) {
        {
            let r_lock = self.machine.read().unwrap();
            assert!(
                match &*r_lock {
                    Machine::NotOperational => true,
                    _ => false,
                }
            );
        }

        let mut lock = self.machine.write().unwrap();
        *lock = Machine::BootstrapCenter(Arc::new(Mutex::new(BootstrapCenter::new(peers_id))));
    }

    pub fn startup_as_bootstrap_peer(&mut self, center_address: SocketAddr) -> Option<TargetedMessage> {
        {
            let r_lock = self.machine.read().unwrap();
            assert!(
                match &*r_lock {
                    Machine::NotOperational => true,
                    _ => false,
                }
            );
        }

        let bsp = BootstrapPeer::new(self.bind_address, center_address);
        let (target, msg) = bsp.generate_hello_request();

        debug!("{:?} CHECKPOINT startup_as_bootstrap_center @ {} {}", self.our_id, module_path!(), line!());

        {
            let mut lock = self.machine.write().unwrap();
            *lock = Machine::BootstrapPeer(Arc::new(Mutex::new(bsp)));
        }
        self.resolve_socket_task(SocketTarget::Addr(target), SocketMessage::BootstrapMessage(msg))
    }

    pub fn startup_as_validator(&mut self) {
        unimplemented!()
    }
}

impl Handler {
    pub fn node_number(&self) -> u64 {
        let lock = self.node_socket_map.read().unwrap();
        lock.len() as u64
    }

    pub fn nodes(&self) -> BinaryHeap<NodeId> {
        let lock = self.node_socket_map.read().unwrap();
        lock.keys().cloned().collect()
    }

    pub fn epoch(&self) -> u64 {
        self.epoch.load(Ordering::Relaxed)
    }

    pub fn our_shard_index(&self) -> u64 {
        compute_node_ascription(
            &self.our_id,
            self.epoch(),
            compute_shard_number(self.node_number()),
        )
    }
}

// Transportation
impl Handler {
    pub fn handle_socket_message(&mut self, sender_pk: PublicKey, message: SocketMessage, timestamp: DateTime<Utc>) -> Vec<TargetedMessage> {
        debug!("In  | {:?}->{:?}: {:?}", NodeId::from_pubkey(&sender_pk), self.our_id, &message);
        let response = match message {
            SocketMessage::Success => {
                Vec::new()
            }
            SocketMessage::ShardMessage(shard_message) => {
                self.handle_shard_message(shard_message)
            }
            SocketMessage::DealerMessage(dealer_message) => {
                self.handle_dealer_message(sender_pk, dealer_message, timestamp)
            }
            SocketMessage::BootstrapMessage(bootstrap_message) => {
                self.handle_bootstrap_message(sender_pk, bootstrap_message, timestamp)
            }
            SocketMessage::ConsensusMessage(consensus_message) => {
                let node_id = NodeId::from_pubkey(&sender_pk);
                self.handle_consensus_message(
                    node_id,
                    consensus_message.consensus_id,
                    consensus_message.dhb_message,
                )
            }
            _ => {
                unreachable!()
            }
        };

        response.into_iter().filter_map(|task| {
            self.resolve_socket_task(task.target, task.message)
        }).collect()
    }

    pub fn resolve_socket_task(&self, target: SocketTarget, message: SocketMessage) -> Option<TargetedMessage> {
        debug!("Out | {:?}->{:?}: {:?}", self.our_id, &target, &message);

        match target {
            SocketTarget::Dealer => {
                Some(TargetedMessage {
                    target: vec![self.dealer_address.clone()],
                    message,
                })
            }
            SocketTarget::Addr(address) => {
                Some(TargetedMessage {
                    target: vec![address],
                    message,
                })
            }
            SocketTarget::NodeId(node_id) => {
                let nsp = self.node_socket_map.read().unwrap();
                if let Some(address) = (*nsp).get(&node_id) {
                    Some(TargetedMessage {
                        target: vec![address.to_owned()],
                        message,
                    })
                } else {
                    error!("No socket address record found for {}", hex::encode(&node_id));
                    None
                }
            }
            SocketTarget::MultiAddr(target) => {
                Some(TargetedMessage { target, message })
            }
            SocketTarget::MultiNodeId(node_ids) => {
                let nsp = self.node_socket_map.read().unwrap();

                let target: Vec<SocketAddr> = node_ids.iter().filter_map(|node_id| {
                    (*nsp).get(node_id)
                }).map(|x| {
                    x.to_owned()
                }).collect();

                Some(TargetedMessage { target, message })
            }
        }
    }
}