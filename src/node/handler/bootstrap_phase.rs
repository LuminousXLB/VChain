#![allow(unused_imports)]

use std::{
    collections::BTreeMap,
    net::SocketAddr,
    sync::{Arc, Mutex},
};
use std::collections::BinaryHeap;
use std::sync::atomic::Ordering;
use std::sync::RwLock;
use std::task::RawWaker;

use chrono::{DateTime, Utc};
use hbbft::crypto::PublicKey;
use log::*;

use crate::{
    messaging::message::SocketMessage,
    node::{
        bootstrap::BootstrapMessage,
        consensus::ConsensusNode,
        handler::{Handler, Machine},
        SocketTarget,
        SocketTransmitTask,
    },
    node::consensus::ValidatorNode,
    NodeId,
};
use crate::blockchain::block::{VChainBlock, VChainBlockHeader, VChainBlockPayload};
use crate::blockchain::sharding::{compute_block_representative, ShardMessage};
use crate::utils::Hash256;

// Bootstrap Phase
impl Handler {
    pub fn handle_bootstrap_message(&mut self, sender_pk: PublicKey, message: BootstrapMessage, _timestamp: DateTime<Utc>)
                                    -> Vec<SocketTransmitTask> {
        match message {
            BootstrapMessage::HelloRequest(sender_addr) => {
                match &mut *self.machine.write().unwrap() {
                    Machine::BootstrapCenter(center) => {
                        let task_option = {
                            let mut center_lock = center.lock().unwrap();
                            let rsp = (*center_lock).handle_hello_request(sender_pk.clone(), sender_addr.clone());

                            if let Ok(count) = rsp {
                                // FIXME: modify the threshold to 15
                                if count >= 15 {
                                    let mut public_key_map = (*center_lock).node_map();
                                    match public_key_map.insert(self.our_id, self.our_pk) {
                                        Some(_) => panic!("Center's Key Occupied"),
                                        None => (),
                                    }

                                    let mut socket_addr_map = (*center_lock).addr_map();
                                    match socket_addr_map.insert(self.our_id, self.bind_address) {
                                        Some(_) => panic!("Center's Key Occupied"),
                                        None => (),
                                    }

                                    let pk_addr_map: BTreeMap<PublicKey, SocketAddr> = public_key_map.iter()
                                        .zip(socket_addr_map.iter())
                                        .map(|((id1, pk), (id2, addr))| {
                                            if id1 != id2 {
                                                panic!("Wrong order");
                                            }

                                            (pk.to_owned(), addr.to_owned())
                                        }).collect();

                                    Some((
                                        SocketTarget::MultiAddr(socket_addr_map.values().cloned().collect()),
                                        BootstrapMessage::Sync(pk_addr_map)
                                    ))
                                } else {
                                    Some((SocketTarget::Addr(sender_addr.clone()), BootstrapMessage::HelloResponse(rsp)))
                                }
                            } else {
                                Some((SocketTarget::Addr(sender_addr.clone()), BootstrapMessage::HelloResponse(rsp)))
                            }
                        };


                        match task_option {
                            None => vec![],
                            Some((target, bootstrap_message)) => {
                                vec![SocketTransmitTask::new(target, SocketMessage::BootstrapMessage(bootstrap_message))]
                            }
                        }
                    }
                    _ => unreachable!()
                }
            }
            BootstrapMessage::HelloResponse(hello_response) => {
                match &*self.machine.read().unwrap() {
                    Machine::BootstrapPeer(peer) => {
                        let peer_lock = peer.lock().unwrap();
                        (*peer_lock).handle_hello_response(hello_response);
                        vec![]
                    }
                    Machine::Validator(_) => vec![],
                    _ => unreachable!()
                }
            }
            BootstrapMessage::Sync(pk_addr_map) => {
                self.bootstrap_sync(pk_addr_map)
            }
            BootstrapMessage::HoneyBadgerStep(_) => {
                unimplemented!()
            }
        }
    }

    fn bootstrap_sync(&mut self, node_map: BTreeMap<PublicKey, SocketAddr>) -> Vec<SocketTransmitTask> {
        let node_socket_map: BTreeMap<_, _> = node_map.iter()
            .map(|(pubkey, sock_addr)| {
                let node_id = NodeId::from_pubkey(pubkey);
                (node_id, sock_addr.to_owned())
            }).collect();

        assert!(!node_socket_map.is_empty());
        {
            let mut lock = self.node_socket_map.write().unwrap();
            *lock = node_socket_map;
        }

        let node_pk_map: BTreeMap<NodeId, PublicKey> = node_map.into_iter()
            .map(|(pubkey, _)| {
                let node_id = NodeId::from_pubkey(&pubkey);
                (node_id, pubkey)
            }).collect();

        self.epoch.fetch_add(1, Ordering::Relaxed);

        let (validator, messages) = ValidatorNode::new(
            self.our_sk.clone(),
            node_pk_map.clone(),
            self.epoch(),
        );

        {
            let mut lock = self.machine.write().unwrap();
            *lock = Machine::Validator(validator);
        }

        debug!("{:?}: Turn to Validator", self.our_id);

        let mut tasks: Vec<_> = messages.into_iter()
            .map(|message| {
                SocketTransmitTask::new(
                    SocketTarget::Dealer,
                    SocketMessage::DealerMessage(message),
                )
            }).collect();

        let node_ids: BinaryHeap<NodeId> = node_pk_map.keys().cloned().collect();
        assert!(!node_ids.is_empty());

        let node_idx = compute_block_representative(
            self.epoch(),
            &Hash256::default(),
            node_ids.len() as u64,
        );

        let nodes = node_ids.into_sorted_vec();

        if nodes[node_idx as usize] == self.our_id {
            let header = VChainBlockHeader::new(0, self.epoch(), Default::default());

            let payload = VChainBlockPayload::CodeBase;

            let block = VChainBlock::new(header, payload, &self.our_sk);

            tasks.push(SocketTransmitTask {
                target: SocketTarget::MultiNodeId(nodes),
                message: SocketMessage::ShardMessage(ShardMessage::Block(block)),
            })
        }

        tasks
    }
}
