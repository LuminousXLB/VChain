use std::net::SocketAddr;

use log::*;

use crate::node::bootstrap::BootstrapMessage;

#[derive(Debug)]
pub struct BootstrapPeer {
    bind_address: SocketAddr,
    center_address: SocketAddr,
}

impl BootstrapPeer {
    pub fn new(bind_address: SocketAddr, center_address: SocketAddr) -> Self {
        Self {
            bind_address,
            center_address,
        }
    }

    pub fn generate_hello_request(&self) -> (SocketAddr, BootstrapMessage) {
        let msg = BootstrapMessage::HelloRequest(self.bind_address.clone());
        (self.center_address.clone(), msg)
    }

    pub fn handle_hello_response(&self, result: Result<usize, String>) {
        match result {
            Ok(count) => info!("There have been {} nodes", count),
            Err(reason) => error!("Error: {}", reason),
        };
    }
}

