use std::{
    collections::{BTreeMap, HashSet},
    net::SocketAddr,
};

use hbbft::crypto::PublicKey;

use crate::NodeId;

#[derive(Debug)]
pub struct BootstrapCenter {
    peers_id: HashSet<NodeId>,
    socket_addr_map: BTreeMap<NodeId, SocketAddr>,
    public_key_map: BTreeMap<NodeId, PublicKey>,
}

impl BootstrapCenter {
    pub fn new(peers_id: HashSet<NodeId>) -> Self {
        BootstrapCenter {
            peers_id,
            socket_addr_map: BTreeMap::new(),
            public_key_map: BTreeMap::new(),
        }
    }

    pub fn handle_hello_request(&mut self, public_key: PublicKey, socket_addr: SocketAddr) -> Result<usize, String> {
        let id = NodeId::from_pubkey(&public_key);

        if self.peers_id.contains(&id) {
            if self.socket_addr_map.contains_key(&id) {
                Err(String::from("Already registered"))
            } else {
                self.public_key_map.insert(id, public_key);
                match self.socket_addr_map.insert(id, socket_addr) {
                    None => Ok(self.socket_addr_map.len()),
                    Some(_) => unimplemented!(),
                }
            }
        } else {
            Err(String::from("Invalid bootstrap member"))
        }
    }

    pub fn addr_map(&self) -> BTreeMap<NodeId, SocketAddr> {
        self.socket_addr_map.clone()
    }

    pub fn node_map(&self) -> BTreeMap<NodeId, PublicKey> {
        self.public_key_map.clone()
    }
}

