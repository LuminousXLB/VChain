use std::{
    collections::BTreeMap,
    net::SocketAddr,
};

use hbbft::{
    crypto::PublicKey,
    dynamic_honey_badger::Message,
};
use serde::{Deserialize, Serialize};

pub use bootstrap_center::BootstrapCenter;
pub use bootstrap_peer::BootstrapPeer;

//use crate::message::InboundMessage;
use crate::NodeId;

mod bootstrap_center;
mod bootstrap_peer;

//pub type SocketMessageTx = mpsc::UnboundedSender<InboundMessage>;
//pub type SocketMessageRx = mpsc::UnboundedReceiver<InboundMessage>;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum BootstrapMessage {
    HelloRequest(SocketAddr),
    HelloResponse(Result<usize, String>),
    Sync(BTreeMap<PublicKey, SocketAddr>),
    HoneyBadgerStep(Message<NodeId>),
}