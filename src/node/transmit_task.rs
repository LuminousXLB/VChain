use std::net::SocketAddr;

use crate::{
    messaging::message::SocketMessage,
    NodeId,
};

#[derive(Debug)]
pub enum SocketTarget {
    Dealer,
    Addr(SocketAddr),
    NodeId(NodeId),
    MultiAddr(Vec<SocketAddr>),
    MultiNodeId(Vec<NodeId>),
}

#[derive(Debug)]
pub struct SocketTransmitTask {
    pub(crate) target: SocketTarget,
    pub(crate) message: SocketMessage,
}

impl SocketTransmitTask {
    pub fn new(target: SocketTarget, message: SocketMessage) -> Self {
        Self { target, message }
    }
}

