use std::collections::{BTreeMap, BTreeSet};

use hbbft::{
    crypto::{PublicKey, SecretKey},
    dynamic_honey_badger::{DynamicHoneyBadgerBuilder, Message},
    NetworkInfo,
    queueing_honey_badger::{QueueingHoneyBadger, QueueingHoneyBadgerBuilder, Result, Step},
};
use log::*;
use rand::thread_rng;

use crate::{
    blockchain::transaction::VChainTransaction,
    ConsensusId,
    dealer::{
        message::{
            DealerMessage,
            KeyRequest,
            KeyResponse,
        }
    },
    NodeId,
};

#[derive(Debug)]
pub struct ConsensusNode {
    pub instance_id: ConsensusId,
    our_id: NodeId,
    our_pk: PublicKey,
    our_sk: SecretKey,
    node_map: BTreeMap<NodeId, PublicKey>,
    message_buffer: Vec<(NodeId, Message<NodeId>)>,
    transaction_buffer: Vec<VChainTransaction>,
    queueing_honey_badger: Option<QueueingHoneyBadger<VChainTransaction, NodeId, Vec<VChainTransaction>>>,
}

impl ConsensusNode {
    pub fn new(our_sk: SecretKey, node_map: BTreeMap<NodeId, PublicKey>) -> (Self, DealerMessage) {
        let our_pk = our_sk.public_key();
        let our_id = NodeId::from_pubkey(&our_pk);

        let node_ids: BTreeSet<NodeId> = node_map.keys().cloned().collect();

        let request = KeyRequest::new(our_id, node_ids.clone());
        let instance_id = ConsensusId::from_slice(request.proof().root_hash.as_slice());

        debug!("{:?} CHECKPOINT NEW CONSENSUS INSTANCE {:?}: {:?}", our_id, instance_id, node_ids);

        (
            Self {
                instance_id,
                our_id,
                our_pk,
                our_sk,
                node_map,
                message_buffer: vec![],
                transaction_buffer: vec![],
                queueing_honey_badger: None,
            },
            DealerMessage::KeyRequest(request)
        )
    }

    pub fn handle_keygen_response(&mut self, key_response: KeyResponse, transactions: Vec<VChainTransaction>)
                                  -> Option<Step<VChainTransaction, NodeId>> {
        if self.instance_id != key_response.group_root_hash {
            panic!("Instance ID doesn't match");
        }

        let network_info = NetworkInfo::new(
            self.our_id.clone(),
            key_response.secret_key_share.into_inner(),
            key_response.public_key_set,
            self.our_sk.clone(),
            self.node_map.clone(),
        );

        let dhb = DynamicHoneyBadgerBuilder::new().build(network_info);

        let result = QueueingHoneyBadgerBuilder::new(dhb)
            .build_with_transactions(transactions, &mut thread_rng());

        match result {
            Ok((mut qhb, mut step)) => {
                let mut rng = thread_rng();
                self.message_buffer.iter().for_each(|(sender_id, message)| {
                    let sub_step = qhb.handle_message(sender_id, message.to_owned(), &mut rng).unwrap();
                    step.extend(sub_step);
                });

                self.queueing_honey_badger = Some(qhb);

                Some(step)
            }
            Err(err) => {
                error!("Error building QHB: {:?}", err);
                None
            }
        }
    }

    pub fn candidates(&self) -> Vec<NodeId> {
        self.node_map.keys().cloned().collect()
    }


    pub fn propose(&mut self, mut transactions: Vec<VChainTransaction>) -> Option<Result<Step<VChainTransaction, NodeId>>> {
        if let Some(qhb) = self.queueing_honey_badger.as_mut() {
            let mut step = Step::default();
            transactions.into_iter().for_each(|tx| {
                let r = qhb.push_transaction(tx, &mut thread_rng()).unwrap();
                step.extend(r);
            });

            Some(Ok(step))
        } else {
            self.transaction_buffer.append(&mut transactions);
            None
        }
    }


    pub fn handle_message(&mut self, sender_id: NodeId, message: Message<NodeId>) -> Option<Result<Step<VChainTransaction, NodeId>>> {
        assert!(self.node_map.contains_key(&sender_id));

        if let Some(qhb) = self.queueing_honey_badger.as_mut() {
            let step = qhb.handle_message(&sender_id, message.clone(), &mut thread_rng());

//            if let Ok(s) = &step {
//                if !s.fault_log.0.is_empty() {
//                    error!("Fault Log our_id={:?}, consensus_id={:?}, message={:?}, fault={:?}", self.our_id, self.instance_id, message, s.fault_log.0);
//                }
//            };

            Some(step)
        } else {
            self.message_buffer.push((sender_id, message));
            None
        }
    }
}