use hbbft::dynamic_honey_badger::Message;
use serde::{Deserialize, Serialize};

pub use consensus_node::ConsensusNode;
pub use validator_node::ValidatorNode;

use crate::{ConsensusId, NodeId};

mod consensus_node;
mod validator_node;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConsensusMessage {
    pub consensus_id: ConsensusId,
    pub dhb_message: Message<NodeId>,
}

