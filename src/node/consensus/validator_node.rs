use std::{
    collections::{BinaryHeap, BTreeMap, BTreeSet, HashMap, HashSet},
    sync::{Arc, Mutex, RwLock},
};

use hbbft::{
    crypto::{PublicKey, SecretKey},
    dynamic_honey_badger::{Batch, Message},
    queueing_honey_badger::{Result, Step},
};
use log::*;

use crate::{
    blockchain::{
        sharding::{
            compute_concerned_shards_indices,
            compute_consensus_id,
            compute_node_ascription,
            compute_shard_number,
            compute_sharding,
            compute_transaction_assignment,
            compute_transaction_representative,
        },
        transaction::VChainTransaction,
    },
    ConsensusId,
    dealer::message::{
        DealerMessage,
        KeyResponse,
    },
    node::consensus::ConsensusNode,
    NodeId,
    utils::Hash256,
};

#[derive(Clone, Debug)]
pub struct ValidatorNode {
    our_id: NodeId,
    our_pk: PublicKey,
    our_sk: SecretKey,
    epoch_id: u64,
    our_shard_index: u64,
    pub our_shard_id: ConsensusId,
    candidates_map: Arc<RwLock<HashMap<NodeId, PublicKey>>>,
    shards_id_map: Arc<RwLock<HashMap<u64, ConsensusId>>>,
    shards_node_map: Arc<RwLock<HashMap<ConsensusId, BinaryHeap<NodeId>>>>,
    consensus_map: Arc<RwLock<HashMap<ConsensusId, Arc<Mutex<ConsensusNode>>>>>,
    message_buffer: Arc<Mutex<HashMap<ConsensusId, Vec<VChainTransaction>>>>,
    first_layer_common_set: Arc<RwLock<HashMap<u64, BTreeSet<VChainTransaction>>>>,
    possible_concerned_shards: Arc<RwLock<HashMap<u64, BinaryHeap<ConsensusId>>>>,
    extern_shard_representatives: Arc<RwLock<HashMap<u64, HashMap<ConsensusId, NodeId>>>>,
    // Key=ConcernedShardIndex, Value=(ShardId, RepresentativeNodeId)
    second_layer_consensus_id_map: Arc<RwLock<HashMap<u64, ConsensusId>>>,
    round_id_map: Arc<RwLock<HashMap<ConsensusId, (u64, u64)>>>,
    // Key=ConsensusId, Value=(ConcernedShardIndex, RoundId)
    pub prepared_for_block: Arc<Mutex<BTreeSet<VChainTransaction>>>,
}

impl ValidatorNode {
    pub fn new(our_sk: SecretKey, candidates: BTreeMap<NodeId, PublicKey>, epoch_id: u64) -> (Self, Vec<DealerMessage>) {
        let our_pk = our_sk.public_key();
        let our_id = NodeId::from_pubkey(&our_pk);

        let node_num = candidates.len() as u64;
        let shard_num = compute_shard_number(node_num);
        let our_shard_index = compute_node_ascription(&our_id, epoch_id, shard_num);

        // compute sharding result
        let mut shards_id_map: HashMap<u64, ConsensusId> = HashMap::new();
        let mut shards_node_map: HashMap<ConsensusId, BinaryHeap<NodeId>> = HashMap::new();
        compute_sharding(candidates.keys().collect(), epoch_id, shard_num).into_iter()
            .for_each(|(shard_index, shard_candidates)| {
                let consensus_id = compute_consensus_id(shard_candidates.clone());
                shards_id_map.insert(shard_index, consensus_id.clone());
                shards_node_map.insert(consensus_id, shard_candidates);
            });

        let our_shard_id = shards_id_map.get(&our_shard_index).unwrap().clone();

        let our_shard_node_map: BTreeMap<NodeId, PublicKey> = shards_node_map.get(&our_shard_id).unwrap().iter()
            .map(|node_id| {
                (node_id.clone(), candidates.get(node_id).unwrap().clone())
            }).collect();

        // compute potential concerned shards
        let possible_concerned_shards: HashMap<u64, BinaryHeap<ConsensusId>> = (0..shard_num).filter_map(|base_index| {
            let concerned = compute_concerned_shards_indices(base_index, node_num);

            if concerned.contains(&our_shard_index) {
                let concerned_shards_id: BinaryHeap<ConsensusId> = concerned.iter().map(|shard_index| {
                    shards_id_map.get(shard_index).expect(format!("Invalid Shard Index {}", shard_index).as_str()).clone()
                }).collect();

                Some((base_index, concerned_shards_id))
            } else {
                None
            }
        }).collect();

        // check 2nd layer consensus representative
        let extern_shard_representatives: HashMap<u64, HashMap<ConsensusId, NodeId>> = possible_concerned_shards.iter()
            .map(|(base_index, concerned_shards)| {
                {
                    let set: HashSet<_> = concerned_shards.iter().collect();
                    assert!(set.contains(&our_shard_id))
                }

                let representatives = concerned_shards.iter().map(|consensus_id| {
                    let shard = shards_node_map.get(consensus_id).unwrap();
                    let node_idx = compute_transaction_representative(
                        0,
                        consensus_id,
                        concerned_shards,
                        shard.len() as u64,
                    );

                    (consensus_id.clone(), shard.iter().nth(node_idx as usize).unwrap().clone())
                }).collect();

                (base_index.clone(), representatives)
            }).collect();

        let round_id_map = extern_shard_representatives.iter()
            .map(|(base_index, representative_map)| {
                let representatives = representative_map.values().cloned().collect();
                let consensus_id = compute_consensus_id(representatives);
                (consensus_id, (base_index.clone(), 0))
            }).collect();

        let repr: Vec<u64> = extern_shard_representatives.iter()
            .filter_map(|(base_index, representative)| {
                if representative.get(&our_shard_id).unwrap() == &our_id {
                    Some(base_index.clone())
                } else {
                    None
                }
            }).collect();

        // build 1st layer consensus instance
        let (consensus, message) = ConsensusNode::new(our_sk.clone(), our_shard_node_map);
        let mut map = HashMap::new();
        map.insert(our_shard_id, Arc::new(Mutex::new(consensus)));

        let mut node = ValidatorNode {
            our_id,
            our_pk,
            our_sk,
            epoch_id,
            our_shard_index,
            our_shard_id,
            candidates_map: Arc::new(RwLock::new(candidates.into_iter().collect())),
            shards_id_map: Arc::new(RwLock::new(shards_id_map)),
            shards_node_map: Arc::new(RwLock::new(shards_node_map)),
            consensus_map: Arc::new(RwLock::new(map)),
            message_buffer: Arc::new(Mutex::new(HashMap::new())),
            first_layer_common_set: Arc::new(RwLock::new(HashMap::new())),
            possible_concerned_shards: Arc::new(RwLock::new(possible_concerned_shards)),
            round_id_map: Arc::new(RwLock::new(round_id_map)),
            extern_shard_representatives: Arc::new(RwLock::new(extern_shard_representatives)),
            second_layer_consensus_id_map: Arc::new(RwLock::new(HashMap::new())),
            prepared_for_block: Arc::new(Mutex::new(BTreeSet::new())),
        };


        let mut messages = vec![message];
        for x in repr {
            messages.push(node.build_2nd_layer_consensus(x, 0));
        }

        (node, messages)
    }

    pub fn node_number(&self) -> u64 {
        let lock = self.candidates_map.read().unwrap();
        lock.len() as u64
    }

    fn build_2nd_layer_consensus(&mut self, concerned_base: u64, round_id: u64) -> DealerMessage {
        let node_map: BTreeMap<NodeId, PublicKey> = {
            let pcs = self.possible_concerned_shards.read().unwrap();
            let concerned_shards = pcs.get(&concerned_base).unwrap();

            concerned_shards.iter().map(|shard_id| {
                let node_id = {
                    let snm = self.shards_node_map.read().unwrap();
                    let shard = snm.get(shard_id).unwrap();

                    let node_index = compute_transaction_representative(round_id, shard_id, concerned_shards, shard.len() as u64);
                    shard.iter().nth(node_index as usize).unwrap().clone()
                };

                let public_key = {
                    let candidates = self.candidates_map.read().unwrap();
                    candidates.get(&node_id).unwrap().clone()
                };

                (node_id, public_key)
            }).collect()
        };

        assert!(node_map.contains_key(&self.our_id));

        let (consensus, message) = ConsensusNode::new(self.our_sk.clone(), node_map);
        let consensus_id = consensus.instance_id.clone();

        {
            let mut map = self.consensus_map.write().unwrap();
            map.insert(consensus_id.clone(), Arc::new(Mutex::new(consensus)));
        }

        {
            let mut second_layer_map = self.second_layer_consensus_id_map.write().unwrap();
            second_layer_map.insert(concerned_base, consensus_id);
        }

        message
    }

    pub fn propose(&mut self, transaction: VChainTransaction) -> Option<Result<Step<VChainTransaction, NodeId>>> {
        let transactions = vec![transaction];
        debug!("{:?} CHECKPOINT: 1st layer propose: {:?}", self.our_id, &transactions);

        let map = self.consensus_map.read().unwrap();
        let consensus = (*map).get(&self.our_shard_id).unwrap();
        let mut lock = consensus.lock().unwrap();
        (*lock).propose(transactions)
    }

    pub fn consensus_candidates(&self, consensus_id: &ConsensusId) -> Vec<NodeId> {
        let map = self.consensus_map.read().unwrap();
        let consensus = (*map).get(&consensus_id).unwrap();
        let lock = consensus.lock().unwrap();
        lock.candidates()
    }

    pub fn handle_keygen_response(&mut self, key_response: KeyResponse) -> Option<Step<VChainTransaction, NodeId>> {
        let consensus_id = key_response.group_root_hash.clone();

        let transactions = {
            let mut lock = self.message_buffer.lock().unwrap();
            (*lock).remove(&consensus_id).unwrap_or_default()
        };

        let map = self.consensus_map.read().unwrap();
        let node_mutex = map.get(&consensus_id).unwrap();
        let mut node = node_mutex.lock().unwrap();

        (*node).handle_keygen_response(key_response, transactions)
    }

    pub fn handle_message(&mut self, consensus_id: &ConsensusId, sender_id: NodeId, message: Message<NodeId>)
                          -> Option<Result<Step<VChainTransaction, NodeId>>> {
        let map = self.consensus_map.read().unwrap();

        match map.get(consensus_id) {
            None => {
                None
            }
            Some(node_mutex) => {
                let mut node = node_mutex.lock().unwrap();
                (*node).handle_message(sender_id, message)
            }
        }
    }

    pub fn handle_1st_layer_consensus_output(&mut self, output: Vec<Batch<Vec<VChainTransaction>, NodeId>>) -> HashMap<ConsensusId, Result<Step<VChainTransaction, NodeId>>> {
        // evaluate 1st layer output
        {
            output.iter()
                .flat_map(|batch| {
                    batch.contributions().map(|(_id, transaction)| {
                        transaction
                    }).cloned().flatten()
                })
                .for_each(|tx| {
                    let shard_num = compute_shard_number(self.node_number());
                    assert_ne!(shard_num, 0);

                    let base_index = compute_transaction_assignment(
                        self.epoch_id,
                        &tx.digest,
                        &Hash256::default(),
                        shard_num,
                    );

                    let mut common_set = self.first_layer_common_set.write().unwrap();
                    match common_set.get_mut(&base_index) {
                        None => {
                            let mut buffer = BTreeSet::new();
                            buffer.insert(tx);
                            common_set.insert(base_index, buffer);
                        }
                        Some(buffer) => {
                            buffer.insert(tx);
                        }
                    };
                });

            info!("{:?} 1st CONSENSUS COMPLETED, COMMON SET => {:?}", self.our_id, self.first_layer_common_set);
        }

        // check if I am the 1st layer representative in my shard
        let slp = self.second_layer_consensus_id_map.read().unwrap();
        if slp.is_empty() {
            debug!("{:?} is not a representative", self.our_id);
            HashMap::new()
        } else {
            slp.iter().filter_map(|(base_index, consensus_id)| {
                let buffer = {
                    let mut common_set = self.first_layer_common_set.write().unwrap();
                    match common_set.get_mut(&base_index) {
                        None => None,
                        Some(_) => {
                            common_set.insert(base_index.clone(), BTreeSet::new())
                        }
                    }
                };

                let step_option = match buffer {
                    None => None,
                    Some(buffer) => if buffer.is_empty() {
                        None
                    } else {
                        let transactions = buffer.into_iter().collect();
                        debug!("{:?} CHECKPOINT: 2nd layer {:?} propose: {:?}", self.our_id, consensus_id, &transactions);

                        let mut consensus_map = self.consensus_map.write().unwrap();
                        let consensus = consensus_map.get_mut(consensus_id)
                            .expect(format!("consensus_map doesn't contain {:?} @ {}", consensus_id, format_args!("{} {}", module_path!(), line!())).as_str());

                        let mut c_lock = consensus.lock().unwrap();
                        debug!("{:?} is a representative with consensus {:?}: candidates={:?}", self.our_id, consensus_id, c_lock.candidates());

                        c_lock.propose(transactions)
                    },
                };

                match step_option {
                    None => None,
                    Some(step) => Some((consensus_id.clone(), step)),
                }
            }).collect()
        }
    }

    pub fn handle_2nd_layer_consensus_output(&mut self, consensus_id: &ConsensusId, output: HashSet<VChainTransaction>) -> Option<DealerMessage> {
        trace!("CHECKPOINT, handle_2nd_layer_consensus_output");

        // FIXME: implement handle_2nd_layer_consensus_output
        // Query concerned_shard_index and Increase round_id
        let (base_index, round_id) = {
            let mut lock = self.round_id_map.write().unwrap();
            let tuple = lock.get_mut(consensus_id)
                .expect(format!("round_id_map doesn't contain {:?} @ {}", consensus_id, format_args!("{} {}", module_path!(), line!())).as_str());
            tuple.1 += 1;
            tuple.clone()
        };

        // Remove from common set
        {
            let mut lock = self.first_layer_common_set.write().unwrap();
            if let Some(buffer) = lock.get_mut(&base_index) {
                if buffer.is_empty() {
                    lock.remove(&base_index);
                } else {
                    output.iter().for_each(|transaction| {
                        buffer.remove(transaction);
                    });
                };
            }
        }

        // Add to prepared transaction
        {
            let mut lock = self.prepared_for_block.lock().unwrap();
            output.into_iter().for_each(|transaction| {
                lock.insert(transaction);
            });
        }

        // Update extern_shard_representatives
        let representatives: HashSet<_> = {
            let mut lock = self.extern_shard_representatives.write().unwrap();
            let representatives_map = lock.get_mut(&base_index)
                .expect(format!("extern_shard_representatives doesn't contain {:?} @ {}", base_index, format_args!("{} {}", module_path!(), line!())).as_str());

            let concerned_shards = representatives_map.keys().cloned().collect();

            representatives_map.iter_mut().for_each(|(shard_id, representative_id)| {
                let snm = self.shards_node_map.read().unwrap();
                let shard_candidates = snm.get(shard_id)
                    .expect(format!("shards_node_map doesn't contain {:?} @ {}", shard_id, format_args!("{} {}", module_path!(), line!())).as_str());


                let node_index = compute_transaction_representative(
                    round_id,
                    shard_id,
                    &concerned_shards,
                    shard_candidates.len() as u64,
                );

                *representative_id = shard_candidates.iter().nth(node_index as usize).unwrap().clone();
            });

            representatives_map.values().cloned().collect()
        };

        let is_repr = representatives.contains(&self.our_id);

        // Update second_layer_consensus_id_map
        if is_repr {
            let concerned_shard_consensus_id = compute_consensus_id(representatives.into_iter().collect());
            let mut lock = self.second_layer_consensus_id_map.write().unwrap();
            lock.insert(base_index, concerned_shard_consensus_id);
        }

        {
            info!("{:?} 2nd CONSENSUS COMPLETED, REMAIN => {:?}", self.our_id, self.first_layer_common_set);
            info!("{:?} 2nd CONSENSUS COMPLETED, PREPARED FOR BLOCKS => {:?}", self.our_id, self.prepared_for_block);
        }

        // Update consensus_map
        if is_repr {
            Some(self.build_2nd_layer_consensus(base_index, round_id))
        } else {
            None
        }
    }
}