#[macro_use]
extern crate log;

use std::{
    collections::{BTreeMap, HashSet},
    io::{
        self,
        Read,
    },
    net::{IpAddr, Ipv4Addr, SocketAddr},
    time::Duration,
};

use clap::{App, Arg};
use env_logger::Env;
use futures::{
    Future,
    sync::oneshot,
};
use hbbft::crypto::{PublicKey, SecretKey};

use vchain::{
    blockchain::transaction::VChainTransaction,
    dealer::VChainDealer,
    node::handler::Handler,
    NodeId,
    simulation::{
        loop_dealer,
        MessageScheduler,
        SimulatedNode,
    },
};

pub fn key_generate() -> (NodeId, (SecretKey, PublicKey)) {
    let secret_key = SecretKey::random();
    let public_key = secret_key.public_key();
    let node_id = NodeId::from_pubkey(&public_key);

    return (node_id, (secret_key, public_key));
}

pub fn gen_peers_id(key_materials: &BTreeMap<NodeId, (SecretKey, PublicKey)>) -> HashSet<NodeId> {
    key_materials.iter().map(|(id, _)| {
        id.to_owned()
    }).collect()
}

fn main() {
    // Initialize logger
    env_logger::Builder::from_env(Env::default().default_filter_or("debug"))
        .format_timestamp_nanos()
        .init();

    let matches = App::new("VChain Electricity Simulation")
        .arg(Arg::with_name("tx_numbers")
            .short("n")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("tx_interval")
            .short("t")
            .required(true)
            .takes_value(true))
        .get_matches();

    let tx_numbers: u64 = matches.value_of("tx_numbers").unwrap().to_string().parse().unwrap();
    let tx_interval: u64 = matches.value_of("tx_interval").unwrap().to_string().parse().unwrap();

    trace!("Initializing Scheduler");
    let client_secret_key = SecretKey::random();
    let client_id = NodeId::from_pubkey(&client_secret_key.public_key());
    let mut scheduler = MessageScheduler::new(client_secret_key);

    trace!("Generating Keys");
    let (dealer_id, (dealer_sk, _pk)) = key_generate();
    let (center_id, (center_sk, _pk)) = key_generate();
    let peers_key: BTreeMap<NodeId, (SecretKey, PublicKey)> = (0..15).map(|_| key_generate()).collect();

    trace!("Initializing Dealer");
    let dealer_address: SocketAddr = SocketAddr::new(IpAddr::from(Ipv4Addr::new(127, 0, 0, 1)), 5000);
    let (dealer, dealer_in, dealer_out) = {
        let dealer_redis = "redis://localhost:6379".to_string();
        let dealer = VChainDealer::new(dealer_sk, dealer_redis);
        let (dealer_out, dealer_in) = scheduler.register(dealer_id, dealer_address);

        (dealer, dealer_in, dealer_out)
    };

    trace!("Initializing BootstrapCenter");
    let center_address = SocketAddr::new(IpAddr::from(Ipv4Addr::new(127, 0, 0, 1)), 9000);

    let mut center = {
        let mut center_handler = Handler::new(
            center_sk.clone(),
            center_address.clone(),
            dealer_address.clone(),
        );
        center_handler.startup_as_bootstrap_center(gen_peers_id(&peers_key));
        let (center_out, center_in) = scheduler.register(center_id, center_address.clone());

        SimulatedNode::new(center_sk, center_handler, center_in, center_out, None)
    };

    trace!("Initializing BootstrapPeer");
    let peer_nodes: Vec<SimulatedNode> = peers_key.iter().enumerate()
        .map(|(idx, (peer_id, (peer_sk, _pk)))| {
            let machine_id: u8 = (100 + idx) as u8;
            let port: u16 = (10000 + idx) as u16;
            let peer_address = SocketAddr::new(IpAddr::from(Ipv4Addr::new(127, 0, 0, machine_id)), port);

            let mut peer_handler = Handler::new(
                peer_sk.clone(),
                peer_address.clone(),
                dealer_address.clone(),
            );
            let init_msg = peer_handler.startup_as_bootstrap_peer(center_address.clone());

            let (peer_out, peer_in) = scheduler.register(peer_id.clone(), peer_address);

            SimulatedNode::new(peer_sk.clone(), peer_handler, peer_in, peer_out, init_msg)
        }).collect();

    trace!("Starting up");
    std::thread::spawn(move || {
        loop_dealer(dealer, dealer_in, dealer_out);
    });

    let mut sched_loop = scheduler.clone();
    std::thread::spawn(move || {
        sched_loop.loop_schedule();
    });

    std::thread::spawn(move || {
        center.loop_run();
    });

    for mut peer in peer_nodes {
        std::thread::spawn(move || {
            peer.loop_run();
        });
    }

    let mut sched_send = scheduler.clone();
    std::thread::spawn(move || {
        std::thread::sleep(Duration::from_secs(5));

        (0..tx_numbers).for_each(|_| {
            let transaction = VChainTransaction::random(64);
            info!("Send Client ({:?}) Message {:?}", client_id, &transaction);
            sched_send.send_transaction(transaction);

            std::thread::sleep(Duration::from_millis(tx_interval));
        });
    });

    let (tx, rx) = oneshot::channel();
    std::thread::spawn(move || {
        info!("Press ENTER to exit...");
        let _ = io::stdin().read(&mut [0]).unwrap();
        tx.send(())
    });
    let _ = rx.wait();
}