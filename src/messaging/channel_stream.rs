#![allow(dead_code)]

use crossbeam_channel::{Receiver, RecvError};
use futures::{Async, Stream};
use log::*;

pub struct ChannelStream<T> {
    receiver: Receiver<T>
}

impl<T> ChannelStream<T> {
    pub fn new(receiver: Receiver<T>) -> Self {
        Self { receiver }
    }
}

impl<T> Stream for ChannelStream<T> {
    type Item = T;
    type Error = RecvError;

    fn poll(&mut self) -> Result<Async<Option<Self::Item>>, Self::Error> {
        trace!("Channel Stream Polled");
        match self.receiver.recv() {
            Ok(item) => Ok(Async::Ready(Some(item))),
            Err(err) => Err(err)
        }
    }
}

