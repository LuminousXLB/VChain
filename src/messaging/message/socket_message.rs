use serde::{Deserialize, Serialize};

use crate::{
    blockchain::sharding::ShardMessage,
    dealer::message::DealerMessage,
    node::{
        bootstrap::BootstrapMessage,
        consensus::ConsensusMessage,
    },
};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum SocketMessage {
    ShardMessage(ShardMessage),
    DealerMessage(DealerMessage),
    BootstrapMessage(BootstrapMessage),
    ConsensusMessage(ConsensusMessage),
    Success,
    Error(ErrorKind),
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum ErrorKind {
    InvalidSignature,
}
