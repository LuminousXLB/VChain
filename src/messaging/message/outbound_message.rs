use chrono::{
    DateTime,
    serde::ts_milliseconds,
    Utc,
};
use serde::{Deserialize, Serialize};

use crate::messaging::{
    message::SocketMessage,
};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct OutboundMessage {
    #[serde(with = "ts_milliseconds")]
    pub timestamp: DateTime<Utc>,
    pub message: SocketMessage,
}

impl OutboundMessage {
    pub fn new(message: SocketMessage) -> Self {
        OutboundMessage {
            timestamp: Utc::now(),
            message,
        }
    }
}
