use std::fmt::{Debug, Formatter, Result};

use hbbft::crypto::{
    SecretKeyShare,
    serde_impl::SerdeSecret,
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct SecretKeyShareSerializable(SerdeSecret<SecretKeyShare>);

impl SecretKeyShareSerializable {
    pub fn new(secret_key_share: SecretKeyShare) -> Self {
        SecretKeyShareSerializable(SerdeSecret(secret_key_share))
    }

    pub fn into_inner(self) -> SecretKeyShare {
        return self.0.into_inner();
    }
}

impl Debug for SecretKeyShareSerializable {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let secret = &self.0;
        write!(f, "{}", format!("{:?}", secret.0))
    }
}

