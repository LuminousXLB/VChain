#![allow(dead_code)]

use std::net::SocketAddr;

use crate::messaging::message::SocketMessage;

#[derive(Debug, Clone)]
pub struct TargetedMessage {
    pub target: Vec<SocketAddr>,
    pub message: SocketMessage,
}
