use hbbft::{
    crypto::PublicKey,
};

use crate::utils::Hash256;

pub mod messaging;
pub mod dealer;
pub mod node;
pub mod simulation;
pub mod blockchain;

pub type NodeId = Hash256;
pub type ConsensusId = Hash256;
pub type BlockHash = Hash256;
pub type TransactionHash = Hash256;

pub mod utils;

impl NodeId {
    pub fn from_pubkey(public_key: &PublicKey) -> Self {
        Hash256::sha3_256(public_key.to_bytes().as_ref())
    }
}


