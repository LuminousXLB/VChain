#!/usr/bin/python3

from pprint import pprint
from sys import argv

from dateutil import parser
from pandas import DataFrame

BLOCK = 1
TRANSACTION = 2


def parse_log(filename):
    with open(filename) as f:
        logs = f.readlines()

    transactions = []
    transaction_dgsts = set()

    current_level = ''
    block_time = ''
    transaction_time = ''

    def extract(s):
        return s.split(': ')[-1].rstrip(',\n')

    for l in logs:
        if 'VChainBlock' in l:
            current_level = BLOCK
        elif 'VChainTransaction' in l:
            current_level = TRANSACTION
        elif 'timestamp' in l:
            if current_level == BLOCK:
                block_time = parser.parse(extract(l)).replace(tzinfo=None)

            elif current_level == TRANSACTION:
                transaction_time = parser.parse(extract(l)).replace(tzinfo=None)
            else:
                print('error timestamp:', l)
                exit(-1)
        elif 'digest' in l:
            digest = extract(l)
            if digest not in transaction_dgsts:
                transaction_dgsts.add(digest)
                delay = block_time - transaction_time
                transactions.append({
                    'digest': digest,
                    'transaction_time': transaction_time,
                    'transaction_time(s)': transaction_time.timestamp(),
                    'block_time': block_time,
                    'block_time(s)': block_time.timestamp(),
                    'delay': delay,
                    'delay(s)': delay.total_seconds()
                })

    return transactions


def describe_result(transactions):
    df = DataFrame(data=transactions)
    description = df[['transaction_time(s)', 'block_time(s)', 'delay(s)']].describe()
    description.loc['range'] = description.loc['max'] - description.loc['min']
    description.loc['tp'] = description.loc['count'] / description.loc['range']

    return description


def main(log_name, out_name):
    transactions = parse_log(log_name)
    pprint(transactions)
    print(len(transactions))

    description = describe_result(transactions)
    out = f'{out_name}_description.xlsx'
    description.to_excel(out)
    print(out)

    print(description)


if __name__ == '__main__':
    main(argv[1], argv[2])
