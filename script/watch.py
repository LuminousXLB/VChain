from sys import argv
from time import ctime, sleep
from uuid import uuid4

from parse_log import parse_log, describe_result, main

target = int(argv[2])
cnt = 0

while cnt < target:
    txes = parse_log(argv[1])
    cnt2 = len(txes)
    print(f'[{ctime()}] count = {len(txes)}')
    if cnt2 != cnt:
        print(describe_result(txes))
        cnt = cnt2
    else:
        sleep(10)

main(argv[1], f'{argv[3]}_{str(uuid4())}')
